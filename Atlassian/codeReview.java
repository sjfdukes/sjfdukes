/**

 * Adds a watcher to all of the supplied issues.
 * If there is partial success, the issues which we can modify will
 * be modified and the ones we cannot will be returned in an ArrayList.
 * @param issues the list of issues to update
 * @param currentUser the user to run the operation as
 * @param watcher the watcher to add to the issues
 * @return an ArrayList<Issue> containing the issues that could not be modified

*/

//Issue = type
public ArrayList<Issue> addWatcherToAll (final ArrayList<Issue> issues, final User
currentUser, final User watcher) {
    // Creates two arrays, one array for issues where the new watcher was successfully added
    // and a second array for the issues that did not get the new watcher
     ArrayList<Issue> successfulIssues = new ArrayList<Issue> ();
     ArrayList<Issue> failedIssues = new ArrayList<Issue> ();

     // for each issue in the issues list do this
     for (Issue issue : issues) {
         if (canWatchIssue(issue, currentUser, watcher)) {
         successfulIssues.add (issue);
         }
         else {
         failedIssues.add (issue);
         }
    } // take our successful list and add the watcher to the issues
    if (!successfulIssues.isEmpty()) {
    watcherManager.startWatching (watcher, successfulIssues);
    }
    return failedIssues;
} // pass an issue and our two users
  // you always have the right to add yourself to a list of watchers,
  //and if you are an admin, you can subscribe other people to watch an issue
  // If either are true you can keep going
private boolean canWatchIssue (Issue issue, User currentUser, User watcher) {
    if (currentUser.equals(watcher) || currentUser.getHasPermissionToModifyWatchers()) {
    return issue.getWatchingAllowed (watcher);
    }
    return false;
}
