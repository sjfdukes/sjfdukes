## Part 1, Spec Review Exercise


1. What is a writable user?
2. If you are not a writable user, how can you tell?
3. What happens when a non writable user attempts to edit their profile?
    - Do they see the Profile Settings menu in the drop down?
    - Do they see it but it is grayed out?
    - If they can see it and click on it, do they still get redirected to the profile page?
        - If they do get redirected, is the "Edit Details" present at all or grayed out on the profile page?
        - Is there a pop up or informational message explaining why they can't edit their profile?
4. Assuming I am a writable user but not an admin or sys admin, and I have the URL to another persons profile what prevents me from editing it?
    - Is the "Edit Profile" button missing or is it grayed out?
5. Assuming I am a writable user but not an admin or a sys admin, and I have the URL directly to the edit page of another persons profile (how sneaky) what happens?
    - Am I able to successfully get to the page but I can't do anything?
    - Do I instantly get an error of some kind?
        - A friendly error, "Hey you can't do that"
        - A non user friendly error like 404 or 500 error
6. If I am editing a users profile (mine or someone elses), when I enter the "Edit Details" popup what is the expected behavior?
    - Do I always get the same number of fields?
    - Are they extra fields if I am a admin or sys admin?
7. What if someone is already editing their own profile and an admin attempts to also edit it?
    - Will the admin get an error?
    - Is there a read only mode?
    - Will it save their changes and then will the second user over write them?
        - Or will the second save fail?
            - Is there an expected error message if my save has failed?
8. As an admin user (either kind) when I change someone else's password will they get notified?
9. Are all fields on the "Edit Details" pop up required?
    - If so, will a user friend error appear letting the user know the problem?
10. Is there validation on the "Edit Details" fields to check that the user name is unique
    - Does it have to be unique?
11. Is there validation on the "Edit Details" fields to check that the Full name is unique
    - Does it have to be unique?
12. Is there validation on the "Edit Details" fields to check that the email address is unique and a valid format (e.g. there is an @ and a .com present)?
    - Does it have to be unique?
